# 表单设计器

# 该演示仅支持表单设计器。 
# 如需使用全部移步至 ，https://gitee.com/itvita/code-generator  ，代码完全开源。
# 撸码不易，如果对你有用，感谢star ，感谢 fork ，感谢 赞赏。

[在线预览地址：https://d.itvita.cn/](https://d.itvita.cn/)

# 如果对你有用，感谢Star 一下。欢迎Fork。

### 发行版
[https://gitee.com/liuqitcn/ant-form-designer/releases/v1.0.0](https://gitee.com/liuqitcn/ant-form-designer/releases/v1.0.0)

## 感谢 JetBrains 提供的许可证证书
https://www.jetbrains.com/?form=MAC

### 作者gitee：
https://gitee.com/liuqitcn/ant-form-designer

### 最近开发了一个个人网站，无聊可以来逛逛 
https://itvita.cn

### 后台框架体验地址
https://admin.itvita.cn/
账号：itvita  密码:itvita.cn

### 我的小程序
[![DvXjsA.jpg](https://s3.ax1x.com/2020/12/07/DvXjsA.jpg)](https://imgchr.com/i/DvXjsA)

## 安装

```
yarn install
```

### 运行

```
yarn run serve
```

### 打包

```
yarn run build
```

### 上传
```
yarn run upload
```

### 作用 说明

1. 通过拖拽生成 ant-design ui 源码

2. 支持行布局

3. 支持实时预览

4. 支持源码预览

5. 支持 json 预览

6. 支持一键复制

7. 支持一键导出

![https://cdn.jsdelivr.net/gh/itvita/liuqiang@master/md/design-table.gif](https://cdn.jsdelivr.net/gh/itvita/liuqiang@master/md/design-table.gif)

[![BJtzLj.jpg](https://s1.ax1x.com/2020/10/29/BJtzLj.jpg)](https://imgchr.com/i/BJtzLj)

[![BJtC8O.png](https://s1.ax1x.com/2020/10/29/BJtC8O.png)](https://imgchr.com/i/BJtC8O)
